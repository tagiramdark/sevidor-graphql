const userResolvers = require("./user");

const rootResolver = {};

const resolvers = [rootResolver, userResolvers];

module.exports = resolvers;
