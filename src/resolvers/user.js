const userData = require("../../MOCK_DATA.json");

const usersResolvers = {
  Query: {
    getAllUsers: () => userData,
    getUser: (parent, { id }) => {
      return userData.filter((user) => user.id === id);
    },
    getUsers: (parent, { pagination }) => {
      const { limit, cursor } = pagination;
      return userData.slice(cursor, cursor + limit);
    },
  },
  Mutation: {
    createUser: (parent, { newUser }) => {
      const createdUser = { id: userData.length + 1, ...newUser };
      userData.push(createdUser);
      return createdUser;
    },
  },
};


module.exports = usersResolvers;
