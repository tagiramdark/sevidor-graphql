const dotenv = require("dotenv");
const path = require("path");
dotenv.config({
  path: path.resolve(__dirname, process.env.NODE_ENV + ".env"),
});

module.exports = {
  NODE_ENV: process.env.NODE_ENV,
  BASE_URL: process.env.BASE_URL,
  PORT: Number(process.env.PORT),
  AUTH0: {
    REQUIRED: process.env.AUTH0_REQUIRED,
    LOGOUT: process.env.AUTH0_LOGOUT,
    SECRET: process.env.AUTH0_SECRET,
    CLIENT_ID: process.env.AUTH0_CLIENT_ID,
    ISSUER_BASE_URL: process.env.AUTH0_ISSUER_BASE_URL,
  },
};
