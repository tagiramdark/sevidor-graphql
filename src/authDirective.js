const { defaultFieldResolver } = require("graphql");
const { mapSchema, getDirective, MapperKind } = require("@graphql-tools/utils");
const { AuthenticationError } = require("apollo-server-errors");
const config = require("./config");

function authDirective(isAuthName, hasScopeName) {
  const typeDirectiveArgumentMaps = {};
  return {
    isAuthDirectiveTransformer: (schema) =>
      mapSchema(schema, {
        [MapperKind.TYPE]: (type) => {
          const authDirective = getDirective(schema, type, isAuthName)?.[0];
          if (authDirective) {
            typeDirectiveArgumentMaps[type.name] = authDirective;
          }
          return undefined;
        },
        [MapperKind.OBJECT_FIELD]: (fieldConfig, _fieldName, typeName) => {
          const authDirective =
            getDirective(schema, fieldConfig, isAuthName)?.[0] ??
            typeDirectiveArgumentMaps[typeName];
          if (authDirective) {
            const { requires } = authDirective;
            if (requires) {
              const { resolve = defaultFieldResolver } = fieldConfig;
              fieldConfig.resolve = function (source, args, context, info) {
                if (context.isAuthenticated) {
                  return resolve(source, args, context, info);
                }
                throw new AuthenticationError(
                  "You must be logged in to query this schema"
                );
              };
              return fieldConfig;
            }
          }
        },
      }),
    hasScopeDirectiveTransformer: (schema) =>
      mapSchema(schema, {
        [MapperKind.TYPE]: (type) => {
          const authDirective = getDirective(schema, type, hasScopeName)?.[0];
          if (authDirective) {
            typeDirectiveArgumentMaps[type.name] = authDirective;
          }
          return undefined;
        },
        [MapperKind.OBJECT_FIELD]: (fieldConfig, _fieldName, typeName) => {
          const authDirective =
            getDirective(schema, fieldConfig, hasScopeName)?.[0] ??
            typeDirectiveArgumentMaps[typeName];
          if (authDirective) {
            const { requires } = authDirective;
            if (requires) {
              const { resolve = defaultFieldResolver } = fieldConfig;
              fieldConfig.resolve = function (source, args, context, info) {
                if (!context.isAuthenticated)
                  throw new AuthenticationError(
                    "You must be logged in to query this schema"
                  );
                if (
                  context.user[
                    `http://${config.BASE_URL}:${config.PORT}/roles`
                  ].includes(requires)
                ) {
                  return resolve(source, args, context, info);
                }
                throw new AuthenticationError(
                  `You must have the ${requires} role to query this schema`
                );
              };
              return fieldConfig;
            }
          }
        },
      }),
  };
}

module.exports = { authDirective };
