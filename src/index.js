const config = require("./config");
const express = require("express");
const apolloserver = require("./apolloserver");
const { auth, requiresAuth } = require("express-openid-connect");

const AUTH_CONFIG = {
  authRequired: config.AUTH0.REQUIRED,
  auth0Logout: config.AUTH0.LOGOUT,
  secret: config.AUTH0.SECRET,
  baseURL: `http://${config.BASE_URL}:${config.PORT}`,
  clientID: config.AUTH0.CLIENT_ID,
  issuerBaseURL: config.AUTH0.ISSUER_BASE_URL,
};

const PORT = config.PORT;

const app = express();

app.use(auth(AUTH_CONFIG));

app.get('/', (req, res) => {
  res.send(req.oidc.isAuthenticated() ? 'Logged in' : 'Logged out')
});

app.get("/profile", requiresAuth(), (req, res) => {
  res.send(JSON.stringify(req.oidc.user));
});

apolloserver.start().then(() => {
  apolloserver.applyMiddleware({ app: app });

  app.listen(PORT, () => {
    console.log(`Listening on PORT ${PORT}`);
  });
});
