const {
  ApolloServerPluginLandingPageGraphQLPlayground,
} = require("apollo-server-core");
const { GraphQLSchema } = require("graphql");
const { makeExecutableSchema } = require("@graphql-tools/schema");
const { loadSchemaSync } = require("@graphql-tools/load");
const { GraphQLFileLoader } = require("@graphql-tools/graphql-file-loader");
const graphqlResolvers = require("./resolvers");
const { ApolloServer } = require("apollo-server-express");
const { authDirective } = require("./authDirective");

const { isAuthDirectiveTransformer, hasScopeDirectiveTransformer } =
  authDirective("isAuthenticated", "hasRole");

let schema = makeExecutableSchema({
  typeDefs: loadSchemaSync("./src/graphql/**/*.graphql", {
    loaders: [new GraphQLFileLoader()],
  }),
  resolvers: graphqlResolvers,
});

schema = isAuthDirectiveTransformer(schema);
schema = hasScopeDirectiveTransformer(schema);

const apolloserver = new ApolloServer({
  schema: schema,
  plugins: [ApolloServerPluginLandingPageGraphQLPlayground()],
  context: ({ req }) => {
    return { user: req.oidc.user, isAuthenticated: req.oidc.isAuthenticated() };
  },
});

module.exports = apolloserver;
